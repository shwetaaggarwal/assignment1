package com.assignment.util;

public interface AppConstant {

	int ADD_NEW_CUSTOMER=1;
	int ADD_NEW_CAR_TO_EXISTING_CUSTOMER=2;
	int LIST_CUSTOMERS_WITH_CARS_SORTED_BY_NAME =3;
	int LIST_CUSTOMERS_BASED_ON_ID=4;
	int GENERATE_PRIZES_FOR_CUSTOMER=5;
	int EXIT=6;
	
	
}

