package com.assignment.controller;

import java.util.*;

import com.assignment.entities.Cars;
import com.assignment.entities.Customer;
import com.assignment.entities.Hyundai;
import com.assignment.entities.Maruti;
import com.assignment.entities.Toyota;
import com.assignment.util.AppConstant;
import com.assignment.entities.Customer;

public class Admin implements AppConstant {

	static Scanner scan = new Scanner(System.in);
	public static Map<Integer, Customer> customerMap = new HashMap<Integer, Customer>();
	static Set<Integer> set = new HashSet<Integer>();

	public static void main(String args[]) {
		do {
			System.out.println();
			System.out.println("1. Add new Customer");
			System.out.println("2.Add new Car to an existing Customer,");
			System.out
					.println("3.List all Customers with their cars sorted by name");
			System.out.println("4.List individual Customer based on ID,");
			System.out.println("5.Generate prizes for customer");
			System.out.println("6.EXIT");
			System.out.println("ENTER YOUR CHOICE");

			int choice = scan.nextInt();

			switch (choice) {
			case ADD_NEW_CUSTOMER:
				addCustomer();
				break;

			case ADD_NEW_CAR_TO_EXISTING_CUSTOMER:
				addCar();
				break;

			case LIST_CUSTOMERS_WITH_CARS_SORTED_BY_NAME:
				listCustomerName();
				break;

			case LIST_CUSTOMERS_BASED_ON_ID:
				listCustomerbyId();
				break;

			case GENERATE_PRIZES_FOR_CUSTOMER:
				generatePrizes();
				break;

			case EXIT:
				System.out.println("Thanks for using");
				System.exit(0);

			default:
				System.out.println("enter valid number");
				break;

			}
		} while (true);

	}

	private static void generatePrizes() {

		Set<Integer> set = customerMap.keySet();
		List<Integer> keyList = new ArrayList<Integer>(set);
		Random rn = new Random();
		Set<Integer> pId = new HashSet<Integer>();
		for (int i = 0; i <= 5; i++) {

			int pSize = rn.nextInt(keyList.size());
			int prize = keyList.get(pSize);

			pId.add(prize);
		}

		Set<Integer> prId = new HashSet<Integer>();
		System.out.println("enter 3 customer ids");
		for (int i = 0; i <= 2; i++) {
			prId.add(scan.nextInt());

		}

		Set<Integer> prizeId = new HashSet<Integer>();

		for (int a : pId) {
			for (int b : prId) {
				if (a == b) {
					prizeId.add(a);

				}
			}
		}

		System.out.println("prizes received by " + prizeId);

	}

	private static void listCustomerbyId() {

		System.out.println("Enter id of customer");
		int custId = scan.nextInt();
		if (customerMap.containsKey(custId)) {
			Customer cust = customerMap.get(custId);
			cust.display();

		} else {
			System.out.println("Id not exist..");

		}

	}

	private static void listCustomerName() {

		Customer c = new Customer();
		List<Customer> customerList = c.displayName();
		for (int i = 0; i < customerList.size(); i++) {
			customerList.get(i).display();

		}

	}

	private static void addCar() {

		System.out.println("Enter id of customer for adding car:");
		int custId = scan.nextInt();

		if (customerMap.containsKey(custId)) {
			System.out.print("Enter Brand name:");
			String brand = scan.next();
			System.out.print("Enter Model:");
			String model = scan.next();
			System.out.print("Enter Car id:");
			int id = scan.nextInt();
			System.out.print("Enter Price:");
			int price = scan.nextInt();

			switch (brand) {

			case "maruti":

				Maruti maruti = new Maruti(id, model, price);
				customerMap.get(custId).getCar().add(maruti);

				break;

			case "toyota":

				Toyota toyota = new Toyota(id, model, price);
				customerMap.get(custId).getCar().add(toyota);

				break;

			case "hyundai":

				Hyundai hyundai = new Hyundai(id, model, price);
				customerMap.get(custId).getCar().add(hyundai);
				break;

			default:
				System.out.println("brand not exists.");
				break;
			}

			System.out.println("Car " + model + " added in "
					+ customerMap.get(custId).custName + " list of cars");
			System.out.println();

		} else {
			System.out.println("Customer does not exist");
			System.out.println();

		}

	}

	private static void addCustomer() {
		System.out.println();
		System.out.println("enter id of customer");
		int custId = scan.nextInt();
		if (customerMap.containsKey(custId)) {
			System.out.println("ID already exists....... try again");
			addCustomer();

		}

		else {
			System.out.println("enter name of customer");
			String custName = scan.next();
			customerMap.put(custId, new Customer(custId, custName));
			System.out.println();
			System.out.println("Customer Added Sucessfully");

		}

	}

}
