package com.assignment.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Comparator;

import com.assignment.controller.Admin;

public class Customer {
   
	int custId;
	public String custName;
	public List<Cars> carList=new ArrayList<Cars>();
    public List<Customer> customerList=new ArrayList<Customer>();
	
	public List<Cars> getCar() 
	{
		return carList;
	}

	
	public Customer() {
		// TODO Auto-generated constructor stub
	}
	
	public Customer(int custId,String custName)
	{
		this.custId=custId;
		this.custName=custName;
			
		
	}
	
	
	
	public  void display()
	{
		
		System.out.println("id of customer :"+custId);
		System.out.println("name of Customer :"+custName);
		System.out.println();
		
		if(carList.size()>0)
		{
			for(int i=0;i<carList.size();i++)
			{

				System.out.println("Car model:"+carList.get(i).model);
				System.out.println("Car Id :"+carList.get(i).id);
				System.out.println("Car Brand "+carList.get(i).brand);
				System.out.println("Car Price:"+carList.get(i).price);
				System.out.println("Car Resale Value :"+carList.get(i).resale);
				System.out.println();
							}
			
		}
		else
		{
			System.out.println("No Cars owned by customer "+custId);
			System.out.println();
			
		}
	}

	
	public ArrayList<Customer> displayName()
	{
		for (Map.Entry<Integer,Customer> cName : Admin.customerMap.entrySet()) 
		{
			customerList.add(cName.getValue());
		}
		Collections.sort(customerList,new Comparator<Customer>() 
				{

			@Override
			public int compare(Customer o1, Customer o2) 
			{
								return o1.custName.compareTo(o2.custName);	
			}
		});
		return (ArrayList<Customer>) customerList;
	}

	
	
	@Override
	public boolean equals(Object obj) {
		return this.custId == ((Customer) obj).custId;
	}
	@Override
	public String toString() {
		return "ID : " + custId + " name:" +custName;
	}
	
}
